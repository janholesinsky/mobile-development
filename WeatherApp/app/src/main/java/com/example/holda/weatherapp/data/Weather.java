package com.example.holda.weatherapp.data;

/**
 * Created by holda on 10.9.2014.
 */
public class Weather {

    private double temperature;
    private double humidity;
    private double temperatureMax;
    private double temperatureMin;
    private String description;
    private String town;
    private final static double KELVIN_CONSTANT = -273.15;
    //constructor
    public Weather(double temperature, double humidity, double temperatureMax, double temperatureMin, String description, String town) {
        super();
        this.temperature = this.ConvertToCelsius(temperature);
        this.humidity = humidity;
        this.temperatureMax = this.ConvertToCelsius(temperatureMax);
        this.temperatureMin = this.ConvertToCelsius(temperatureMin);
        this.description = description;
        this.town = town;
    }
// change to celsius
    private double ConvertToCelsius(double temperature){
        return (temperature + KELVIN_CONSTANT);
    }
    //getters and setters
    public double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }


}
