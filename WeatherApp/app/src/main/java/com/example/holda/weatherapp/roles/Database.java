package com.example.holda.weatherapp.roles;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.holda.weatherapp.data.WeatherCutted;

/**
 * Created by holda on 10.9.2014.
 */
//https://bitbucket.org/gtl-hig/imt3662_sql_notes/src reuse
public class Database extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "history.db";
    public static final int DATABASE_VERSION = 1;

    public Database(Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(WeatherCutted.WEATHER_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }
}
