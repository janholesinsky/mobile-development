package com.example.holda.weatherapp.roles;

import android.os.AsyncTask;

import com.example.holda.weatherapp.WeatherActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import com.example.holda.weatherapp.data.Weather;
import com.example.holda.weatherapp.data.WeatherCutted;

/**
 * Created by holda on 10.9.2014.
 */

// reuse http://stackoverflow.com/questions/9605913/how-to-parse-json-in-android
// reuse http://www.survivingwithandroid.com/2013/05/build-weather-app-json-http-android.html
public class JsonParsing extends AsyncTask<String, Integer, String> {

    private final static double KELVIN_CONST = -273.15;
    private WeatherActivity wetac;

    public JsonParsing(WeatherActivity wetac) {
        super();
        this.wetac = wetac;
    }
    //downloading from server
    @Override
    protected String doInBackground(String... strings) {
        try {
            String returningValue = Network.downloadFromServer(strings);
            return returningValue;

        } catch (Exception e){
            return new String();
        }
    }

    @Override
    protected void onPostExecute(String value) {
        Weather weather = null;
        WeatherCutted weatherCutted = null;
        //getting objects and parsing
        try {
            JSONObject respObj = new JSONObject(value);
            JSONObject mainObj = respObj.getJSONObject("main");
            double humidity = mainObj.getDouble("humidity");
            double temperature = mainObj.getDouble("temp");
            double temperatureMax = mainObj.getDouble("temp_max");
            double temperatureMin = mainObj.getDouble("temp_min");
            String town = respObj.getString("name");
            JSONArray weatherArray = respObj.getJSONArray("weather");
            String description = weatherArray.getJSONObject(0).getString("description");

            weather = new Weather(temperature,humidity,temperatureMax,temperatureMin,description,town);
            weatherCutted = new WeatherCutted(town,temperature,temperatureMax,temperatureMin,new Date().toString());



        } catch (JSONException e){
            e.printStackTrace();
        }
        this.wetac.setData(weather,weatherCutted);
    }
}
