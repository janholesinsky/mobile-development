package com.example.holda.weatherapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import com.example.holda.weatherapp.Adapters.DatabaseAdapter;
import com.example.holda.weatherapp.data.WeatherCutted;
import com.example.holda.weatherapp.roles.Database;

/**
 * Created by holda on 10.9.2014.
 */
public class ShowDatabase extends Activity {

    Context context;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_layout);
        context = this;
// calling adapter
        DatabaseAdapter databaseAdapter = new DatabaseAdapter(context, WeatherCutted.getAll(new Database(this)));
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(databaseAdapter);






    }
}
