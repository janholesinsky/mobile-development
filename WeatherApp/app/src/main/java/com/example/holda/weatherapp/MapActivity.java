package com.example.holda.weatherapp;


import android.app.Activity;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;


/**
 * Created by holda on 15.9.2014.
 */
public class MapActivity extends Activity {
// reuse https://developers.google.com/maps/documentation/android/start#getting_the_google_maps_android_api_v2
    private GoogleMap googleMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_layout);
        //sets up the map
        googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        googleMap.setMyLocationEnabled(true);
    }
}

