package com.example.holda.weatherapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.holda.weatherapp.R;

import com.example.holda.weatherapp.data.WeatherCutted;

/**
 * Created by holda on 11.9.2014.
 */

// reuse http://theopentutorials.com/tutorials/android/listview/android-custom-listview-with-image-and-text-using-baseadapter/
public class DatabaseAdapter extends BaseAdapter {

    private Context context;
    WeatherCutted[] wetCut;

    public DatabaseAdapter(Context context, WeatherCutted[] wetCut) { //getting array
        super();
        this.context = context;
        this.wetCut = wetCut;
    }

    @Override
    public int getCount() {
        return wetCut.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.single_line_layout, null);

        String town = wetCut[i].getTown();
        double temperature = wetCut[i].getTemperature();
        String date = wetCut[i].getDate();
        double maxTemp = wetCut[i].getTemperatureMax();
        double minTemp = wetCut[i].getTemperatureMin();

        TextView adDate = (TextView) view.findViewById(R.id.textViewDate);
        TextView adTown = (TextView) view.findViewById(R.id.textViewTown);
        TextView adTemp = (TextView) view.findViewById(R.id.textViewTemp);
        TextView adMax = (TextView) view.findViewById(R.id.textViewTempMax);
        TextView adMin = (TextView) view.findViewById(R.id.textViewTempMin);

        adDate.setText(date);
        adTown.setText(town);
        adTemp.setText(temperature+" °C");
        adMax.setText(maxTemp+" °C");
        adMin.setText(minTemp+" °C");

        return view;
    }
}
