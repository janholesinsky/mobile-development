package com.example.holda.weatherapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.holda.weatherapp.data.Weather;
import com.example.holda.weatherapp.data.WeatherCutted;
import com.example.holda.weatherapp.roles.Database;
import com.example.holda.weatherapp.roles.JsonParsing;

//reuse http://developer.android.com/guide/topics/location/strategies.html
public class WeatherActivity extends Activity {


    private LocationManager locManager;
    Weather data;
    private String latitude;
    private String longitude;
    Database database = null;
    TextView weatherOutput;
    Button btnWeatherOutput;
    Button btnDatabase;
    Button btnMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        btnWeatherOutput = (Button) findViewById(R.id.buttonShowWeather);
        weatherOutput = (TextView) findViewById(R.id.textViewOut);
        btnDatabase = (Button) findViewById(R.id.buttonDatabase);
        btnMap = (Button) findViewById(R.id.buttonMap);

    //button listeners
        btnWeatherOutput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JsonParsing parser = new JsonParsing(WeatherActivity.this);

                String params[] = {getLatitude(), getLongitude()};
                parser.execute(params);


            }
        });
        btnDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WeatherActivity.this, ShowDatabase.class);
                WeatherActivity.this.startActivity(intent);

            }
        });
        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WeatherActivity.this, MapActivity.class);
                WeatherActivity.this.startActivity(intent);
            }
        });
        //setting up location
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location location = locManager.getLastKnownLocation(locManager.getBestProvider(new Criteria(), false));
        setLongitude(Double.toString(location.getLongitude()));
        setLatitude(Double.toString(location.getLatitude()));

        LocationListener locListener = new LocationListener() {


            @Override
            public void onLocationChanged(Location location) {
                setLongitude(Location.convert(location.getLongitude(), Location.FORMAT_DEGREES));
                setLatitude(Location.convert(location.getLatitude(), Location.FORMAT_DEGREES));
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }


        };
    // getting updates of location
        locManager.requestLocationUpdates(locManager.getBestProvider(new Criteria(), false), 25000, 0, locListener);
        database = new Database(this);



    }
//getters and setters
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public TextView getWeatherOutput() {
        return weatherOutput;
    }

    public void setWeatherOutput(TextView weatherOutput) {
        this.weatherOutput = weatherOutput;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    public void setData(Weather data, WeatherCutted cuttedData){

        this.data = data;
        weatherOutput.setText(
                "weather in: " + this.data.getTown() + "\n"
               +"present temperature: " + String.format("%.1f", this.data.getTemperature())+" °C" + "\n"
               +"temperature today will be between : " + String.format("%.1f", this.data.getTemperatureMax())+" °C "
               +"and " +String.format("%.1f", this.data.getTemperatureMin())+"  °C\n"
               +"description: "+this.data.getDescription()+ "\n"
               +"humidity: " +String.format("%.1f", this.data.getHumidity())+ " %\n"



        );

        cuttedData.saveToDatabase(database);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
