package com.example.holda.weatherapp.roles;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by holda on 10.9.2014.
 */

// reuse http://www.survivingwithandroid.com/2013/05/build-weather-app-json-http-android.html
public class Network {

    private static byte[] buffer = new byte[512];
    private static final String OpenWeatherURL = "http://api.openweathermap.org/data/2.5/weather?";

    protected static synchronized String downloadFromServer (String... param) throws Exception {

        String returningVaule;
        String url = OpenWeatherURL + "lat=" + param[0] + "&lon=" + param[1];

        //getting connections and downloading info
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        try {
            int readCount;
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            InputStream ist = entity.getContent();

            ByteArrayOutputStream content = new ByteArrayOutputStream();

            while ((readCount = ist.read(buffer)) != -1) {
                content.write(buffer, 0, readCount);
            }

            returningVaule = new String (content.toByteArray());

        } catch (Exception e) {
            throw new Exception(e);
        }

        return returningVaule;
    }


}

