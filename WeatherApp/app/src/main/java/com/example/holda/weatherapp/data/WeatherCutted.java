package com.example.holda.weatherapp.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.holda.weatherapp.roles.Database;

import java.util.ArrayList;
import java.util.List;





/**
 * Created by holda on 10.9.2014.
 */

// reuse https://bitbucket.org/gtl-hig/imt3662_sql_notes/src
public class WeatherCutted {
    // columns and constant
    private final static double KELVIN_CONSTANT = -273.15;
    static final String ID = "id";
    final static String TABLE_NAME = "temperatures";
    static final String TOWN = "city";
    static final String TEMPERATUER_MAX = "temperatureMaximum";
    static final String TEMPERATUER_MIN = "temperatureMinimum";
    static final String TEMPERATURE = "temperature";
    static final String DATE = "date";
//database staff
    public static final String WEATHER_CREATE_TABLE = "CREATE TABLE " + WeatherCutted.TABLE_NAME + " ("
            + WeatherCutted.ID + " INTEGER PRIMARY KEY,"
            + WeatherCutted.TOWN + " TEXT,"
            + WeatherCutted.TEMPERATURE + " REAL,"
            + WeatherCutted.TEMPERATUER_MAX + " REAL,"
            + WeatherCutted.TEMPERATUER_MIN + " REAL,"
            + WeatherCutted.DATE + " TEXT"
            + ");";

    long id;
    String town;
    double temperature;
    double temperatureMax;
    double temperatureMin;
    String date;
    //getters and setters
    public double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    private double ConvertToCelsius(double temperature){
        return (temperature + KELVIN_CONSTANT);
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    //constructors
    private WeatherCutted() {};

    public WeatherCutted(String town, double temperature, double temperatureMax, double temperatureMin, String date) {
        this.town = town;
        this.temperature = ConvertToCelsius(temperature);
        this.temperatureMax = ConvertToCelsius(temperatureMax);
        this.temperatureMin = ConvertToCelsius(temperatureMin);
        this.date = date;
    }
    //saving
    public void saveToDatabase(Database db){
        final ContentValues values = new ContentValues();
        values.put(TOWN, this.town);
        values.put(TEMPERATURE, this.temperature);
        values.put(TEMPERATUER_MAX, this.temperatureMax);
        values.put(TEMPERATUER_MIN, this.temperatureMin);
        values.put(DATE, this.date.toString());


        final SQLiteDatabase dbTemp = db.getReadableDatabase();
        this.id = dbTemp.insert(TABLE_NAME, null, values);
        dbTemp.close();

    }

    public static WeatherCutted[] getAll(final Database db){
        final List<WeatherCutted> weatherList = new ArrayList<WeatherCutted>();
        final SQLiteDatabase dbTemp = db.getWritableDatabase();
        final Cursor c = dbTemp.query(TABLE_NAME, new String[] { ID, TOWN, TEMPERATURE, TEMPERATUER_MAX, TEMPERATUER_MIN, DATE}, null, null, null, null, null);

        c.moveToFirst();
        //adding to list
        while(!c.isAfterLast()){
            final WeatherCutted weather = cursorToTemperature(c);
            weatherList.add(weather);
            c.moveToNext();
        }

        c.close();
        return weatherList.toArray(new WeatherCutted[weatherList.size()]);
    }
        //parsing

    public static WeatherCutted cursorToTemperature(Cursor c) {
        final WeatherCutted weather = new WeatherCutted();
        weather.setTown(c.getString(c.getColumnIndex(TOWN)));
        weather.setTemperature(Double.parseDouble(c.getString(c.getColumnIndex(TEMPERATURE))));
        weather.setTemperatureMax(Double.parseDouble(c.getString(c.getColumnIndex(TEMPERATUER_MAX))));
        weather.setTemperatureMin(Double.parseDouble(c.getString(c.getColumnIndex(TEMPERATUER_MIN))));
        weather.setDate(c.getString(c.getColumnIndex(DATE)));





        return weather;
    }
}
